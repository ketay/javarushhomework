package com.javarush.test.level15.lesson12.bonus03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.HashMap;

/* Факториал
Написать метод, который вычисляет факториал - произведение всех чисел от 1 до введенного числа включая его.
Пример: 4! = factorial(4) = 1*2*3*4 = 24
1. Ввести с консоли число меньше либо равно 150.
2. Реализовать функцию  factorial.
3. Если введенное число меньше 0, то вывести 0.
0! = 1
*/

public class Solution
{
    static HashMap<Integer, BigInteger> cache = new HashMap<Integer, BigInteger>();

    public static void main(String[] args) throws IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int input = Integer.parseInt(reader.readLine());
        reader.close();

        System.out.println(factorial(input));
    }

    public static String factorial(int n)
    {
        //add your code here
        BigInteger result;
        result = BigInteger.ONE;

        if (n < 0)
        {
            result = BigInteger.ZERO;
        } else if (n == 0)
        {
            result = BigInteger.ONE;
        }  else if (null != cache.get(n))
        {
            result = cache.get(n);
        } else if (n > 0 && n <= 150)
        {
            result = BigInteger.valueOf(n).multiply(new BigInteger(factorial(n - 1)));
            cache.put(n, result);
        }

        return result.toString();
    }
}
