package com.javarush.test.level15.lesson12.bonus01;

public class Plane implements Flyable
{
    public int passengerAmount = 0;

    public Plane(int passengerAmount) { this.passengerAmount = passengerAmount; }

    public void fly() {}
}
