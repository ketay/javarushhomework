package com.javarush.test.level04.lesson02.task01;

/**
 * Created by potapov on 08.06.2015.
 */
public class Solution
{
    public static void main(String[] args) throws Exception
    {
        Cat cat = new Cat();
        cat.setName("test");
        System.out.println(cat.name);
    }
}
