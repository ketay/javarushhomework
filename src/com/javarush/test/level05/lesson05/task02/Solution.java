package com.javarush.test.level05.lesson05.task02;

/**
 * Created by potapov on 09.06.2015.
 */
public class Solution
{
    public static void main(String[] args) throws Exception
    {
        Cat cat1 = new Cat();
        cat1.name = "test1";
        cat1.strength = 10;
        Cat cat2 = new Cat();
        cat2.name = "test2";
        cat2.strength = 9;

        System.out.println(cat1.fight(cat2));
        System.out.println(cat2.fight(cat1));
    }
}
