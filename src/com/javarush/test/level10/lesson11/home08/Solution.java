package com.javarush.test.level10.lesson11.home08;

import java.util.ArrayList;

/* Массив списков строк
Создать массив, элементами которого будут списки строк. Заполнить массив любыми данными и вывести их на экран.
*/

public class Solution
{
    public static void main(String[] args)
    {
        ArrayList<String>[] arrayOfStringList =  createList();
        printList(arrayOfStringList);
    }

    public static ArrayList<String>[] createList()
    {
        //напишите тут ваш код
        ArrayList<String>[] result = new ArrayList[3];
        ArrayList<String> list1 = new ArrayList<String>();
        list1.add("1");

        ArrayList<String> list2 = new ArrayList<String>();
        list2.add("2");
        list2.add("3");

        ArrayList<String> list3 = new ArrayList<String>();
        list3.add("4");
        list3.add("5");
        list3.add("6");

        result[0] = list1;
        result[1] = list2;
        result[2] = list3;

        return result;
    }

    public static void printList(ArrayList<String>[] arrayOfStringList)
    {
        for (ArrayList<String> list: arrayOfStringList)
        {
            for (String s : list)
            {
                System.out.println(s);
            }
        }
    }
}