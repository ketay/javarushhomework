package com.javarush.test.level19.lesson10.home02;

/* Самый богатый
В метод main первым параметром приходит имя файла.
В этом файле каждая строка имеет следующий вид:
имя значение
где [имя] - String, [значение] - double. [имя] и [значение] разделены пробелом

Для каждого имени посчитать сумму всех его значений
Вывести в консоль имена, у которых максимальная сумма
Имена разделять пробелом либо выводить с новой строки
Закрыть потоки. Не использовать try-with-resources

Пример входного файла:
Петров 0.501
Иванов 1.35
Петров 0.85

Пример вывода:
Петров
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        BufferedReader fileReader = new BufferedReader(new FileReader(args[0]));
        Map<String, Double> fileMap = new TreeMap<String, Double>();
        String[] stringArray;
        String input;
        while ((input = fileReader.readLine()) != null) {
            stringArray = input.split(" ");
            Double value = fileMap.get(stringArray[0]);
            if ( value != null) {
                fileMap.put(stringArray[0], value + Double.parseDouble(stringArray[1]));
            }
            else {
                fileMap.put(stringArray[0], Double.parseDouble(stringArray[1]));
            }
        }
        fileReader.close();

        Double maxValueInMap=(Collections.max(fileMap.values()));

        for (Map.Entry<String, Double> entry : fileMap.entrySet()) {
            if (entry.getValue().equals(maxValueInMap)) {
                System.out.println(entry.getKey());
            }
        }
    }
}
