package com.javarush.test.level03.lesson06.task03;

/* Семь цветов радуги
Выведи на экран текст - семь цветов радуги.
В каждой строке должно быть по три цвета, в последней - один.
Цвета в строках разделяй пробелом.
Запомни: буквы Ё в тестах нигде нет.
Чтобы тесты проходили правильно, во всех задачах заменяй Ё на Е.
*/

public class Solution
{
    public static void main(String[] args)
    {
        //Напишите тут ваш код
        System.out.println("красный оранжевый желтый");
        System.out.println("зеленый голубой синий");
        System.out.println("фиолетовый");

//        String[] colors = new String[]{"Красный", "оранжевый", "желтый", "зеленый", "голубой", "синий", "фиолетовый"};
//
//        for (int i = 1; i <= 7; i++)
//        {
//            System.out.print(colors[i - 1]);
//            if (i % 3 == 0) System.out.println();
//            else System.out.print(" ");
//        }
    }
}