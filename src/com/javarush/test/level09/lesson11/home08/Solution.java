
package com.javarush.test.level09.lesson11.home08;

import java.util.ArrayList;
import java.util.Scanner;

/* Список из массивов чисел
Создать список, элементами которого будут массивы чисел.
Добавить в список пять объектов–массивов длиной 5, 2, 4, 7, 0 соответственно.
Заполнить массивы любыми данными и вывести их на экран.
*/

public class Solution
{
    public static void main(String[] args)
    {
        ArrayList<int[]> list = createList();
        printList(list);
    }

    public static ArrayList<int[]> createList()
    {
        //напишите тут ваш код
        ArrayList<int[]> result = new ArrayList<int[]>();

        {
            int [] arr = new int[5];
            arr[0] = 10;
            arr[1] = 11;
            arr[2] = 12;
            arr[3] = 13;
            arr[4] = 14;

            result.add(arr);
        }

        {
            int [] arr = new int[2];
            arr[0] = 20;
            arr[1] = 21;

            result.add(arr);
        }

        {
            int [] arr = new int[4];
            arr[0] = 30;
            arr[1] = 31;
            arr[2] = 32;
            arr[3] = 33;

            result.add(arr);
        }

        {
            int [] arr = new int[7];
            arr[0] = 40;
            arr[1] = 41;
            arr[2] = 42;
            arr[3] = 43;
            arr[4] = 44;
            arr[5] = 45;
            arr[6] = 46;

            result.add(arr);
        }

        {
            int [] arr = new int[0];

            result.add(arr);
        }

        return result;
    }

    public static void printList(ArrayList<int[]> list)
    {
        for (int[] array: list )
        {
            for (int x: array)
            {
                System.out.println(x);
            }
        }
    }
}
