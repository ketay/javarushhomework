package com.javarush.test.level09.lesson11.home04;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

/* Конвертер дат
Ввести с клавиатуры дату в формате «08/18/2013»
Вывести на экран эту дату в виде «AUG 18, 2013».
Воспользоваться объектом Date и SimpleDateFormat.
*/

public class Solution
{

    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код
        DateFormat givenDateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.US);
        DateFormat resultDateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        Scanner scanner = new Scanner(System.in);
        Date givenDate = new Date();

        try
        {
            givenDate = givenDateFormat.parse(scanner.next());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        try
        {
            System.out.println(resultDateFormat.format(givenDate).toUpperCase());
        }
        catch (Exception e)
        {
            System.out.println("Exception in converting given to result");
        }
    }
}
