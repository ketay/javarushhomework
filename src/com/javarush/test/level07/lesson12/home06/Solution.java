package com.javarush.test.level07.lesson12.home06;

/* Семья
Создай класс Human с полями имя(String), пол(boolean),возраст(int), отец(Human), мать(Human).
Создай объекты и заполни их так, чтобы получилось:
Два дедушки, две бабушки, отец, мать, трое детей. Вывести объекты на экран.
Примечание:
Если написать свой метод String toString() в классе Human,
то именно он будет использоваться при выводе объекта на экран.
Пример вывода:
Имя: Аня, пол: женский, возраст: 21, отец: Павел, мать: Катя
Имя: Катя, пол: женский, возраст: 55
Имя: Игорь, пол: мужской, возраст: 2, отец: Михаил, мать: Аня
…
*/

public class Solution
{
    public static void main(String[] args)
    {
        //Написать тут ваш код
        Human gp1 = new Human("gf1", true, 55);
        System.out.println(gp1);
        Human gp3 = new Human("gf2", true, 65);
        System.out.println(gp3);
        Human gp2 = new Human("gm1", false, 53);
        System.out.println(gp2);
        Human gp4 = new Human("gm2", false, 56);
        System.out.println(gp4);

        Human p1 = new Human("f1", true, 32, gp1, gp2);
        System.out.println(p1);
        Human p2 = new Human("m1", false, 33, gp3, gp4);
        System.out.println(p2);

        Human c1 = new Human("c1", true, 14, p1, p2);
        System.out.println(c1);
        Human c2 = new Human("c2", false, 12, p1, p2);
        System.out.println(c2);
        Human c3 = new Human("c3", false, 11, p1, p2);
        System.out.println(c3);
    }

    public static class Human
    {
        //Написать тут ваш код
        public String name;
        public boolean sex;
        public int age;
        public Human father;
        public Human mother;

        public Human(String name, boolean sex, int age)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public Human(String name, boolean sex, int age, Human father, Human mother)
        {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.father = father;
            this.mother = mother;
        }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            if (this.father != null)
                text += ", отец: " + this.father.name;

            if (this.mother != null)
                text += ", мать: " + this.mother.name;

            return text;
        }
    }

}
