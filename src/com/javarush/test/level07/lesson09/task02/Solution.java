package com.javarush.test.level07.lesson09.task02;

import java.util.ArrayList;
import java.util.Scanner;

/* 5 слов в обратном порядке
Введи с клавиатуры 5 слов в список строк. Выведи их в обратном порядке.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //Напишите тут ваш код
        ArrayList<String> words = new ArrayList<String>();
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 5; i++)
        {
            words.add(scanner.nextLine());
        }

        for (int i = 4; i >= 0; i--)
        {
            System.out.println(words.get(i));
        }
    }
}
