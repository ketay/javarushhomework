package com.javarush.test.level07.lesson09.task04;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/* Буква «р» и буква «л»
1. Создай список слов, заполни его самостоятельно.
2. Метод fix должен:
2.1. удалять из списка строк все слова, содержащие букву «р»
2.2. удваивать все слова содержащие букву «л».
2.3. если слово содержит и букву «р» и букву «л», то оставить это слово без изменений.
2.4. с другими словами ничего не делать.
Пример:
роза
лира
лоза
Выходные данные:
лира
лоза
лоза
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));

        ArrayList<String> list = new ArrayList<String>();

        String ex;

        while (true)
        {
            ex = bis.readLine();
            if (ex.equals("exit") || ex.equals("выход"))
            {
                break;
            }
            list.add(ex);
        }

        list = fix(list);

        for (String s : list)
        {
            System.out.println(s);
        }
    }

    public static ArrayList<String> fix(ArrayList<String> list)
    {
        //add your code here -  добавь код тут
        ArrayList<String> fixed = new ArrayList<String>();

        for (int i = 0; i < list.size(); i++)
        {
            String s = list.get(i);

            if (s.indexOf("р") == -1 && s.indexOf("л") != -1)
            {
                fixed.add(s);
                fixed.add(s);
            }
            if (s.indexOf("р") != -1 && s.indexOf("л") != -1)
            {
                fixed.add(s);
            }
            if (s.indexOf("р") == -1 && s.indexOf("л") == -1)
            {
                fixed.add(s);
            }
        }

        return fixed;
    }
}