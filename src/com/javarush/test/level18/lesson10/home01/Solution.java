package com.javarush.test.level18.lesson10.home01;

/* Английские буквы
В метод main первым параметром приходит имя файла.
Посчитать количество букв английского алфавита, которое есть в этом файле.
Вывести на экран число (количество букв)
Закрыть потоки. Не использовать try-with-resources
*/

import java.io.FileInputStream;
import java.io.IOException;

public class Solution {
    public static void main(String[] args) throws IOException{
        String fileName = args[0];
        int charCount = 0;

        FileInputStream inputStream = new FileInputStream(fileName);
        byte[] buffer = new byte[inputStream.available()];

        if (inputStream.available() > 0) {
            inputStream.read(buffer);
        }

        inputStream.close();

        for (byte aBuffer : buffer)
        {
            if ((aBuffer > 64 && aBuffer < 91) || (aBuffer > 96 && aBuffer < 123))
                charCount++;
        }

        System.out.print(charCount);
    }
}
